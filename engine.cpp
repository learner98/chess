#include <iostream>
#include <ctime> 
#include <conio.h> 
#include <Windows.h>
#include <algorithm>
#include "SerialPort.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;
//=========================================================================================================================//
int player = 2; // user 
int  y, y2; char x1, x2;
int  col, col2;
char *port_name = "\\\\.\\COM3";
char output[MAX_DATA_LENGTH];
char incomingData[MAX_DATA_LENGTH];
SerialPort arduino(port_name);
//=========================================================================================================================//
void switch_role() {
	(player == 2) ? player = 1 : player = 2;
}
//=========================================================================================================================//
char board[10][10] = {
	{ 'x','x','x','x','x','x','x','x','x','x' },
	{ 'x','R','N','B','Q','K','B','N','R','x' },
	{ 'x','P','P','P','P','P','P','P','P','x' },
	{ 'x','e','e','e','e','e','e','e','e','x' },
	{ 'x','e','e','e','e','e','e','e','e','x' },
	{ 'x','e','e','e','e','e','e','e','e','x' },
	{ 'x','e','e','e','e','e','e','e','e','x' },
	{ 'x','p','p','p','p','p','p','p','p','x' },
	{ 'x','r','n','b','k','q','b','n','r','x' },
	{ 'x','x','x','x','x','x','x','x','x','x' }
};
//=========================================================================================================================//
void draw() {
	system("cls");
	cout << "     A B C D E F G H " << endl;
	for (int i = 0; i < 10; i++) {
		if (i == 0 || i == 9) cout << "   ";
		else {
			cout.width(3);  cout << i;
		}
		for (int j = 0; j < 10; j++) {

			if (board[i][j] == 'x') cout << char(219) << " ";
			else if (board[i][j] == 'e') cout << "  ";
			else cout << board[i][j] << " ";
		}
		cout << endl;
	}

}
//=========================================================================================================================//
void move() {

	char temp = board[y][col]; 
	if (board[y2][col2] == 'e') { 
		board[y][col] = board[y2][col2];
	}
	else board[y][col] = 'e';
	board[y2][col2] = temp;

}
//=========================================================================================================================//
// algorithms to be added
//=========================================================================================================================//
bool check_legal() {
	///////////////////////////validating the format of user input//////////////////////
	//translates user input
	if (x1 == 'a')col = 1;       else if (x1 == 'f')col = 6;
	else if (x1 == 'b')col = 2;  else if (x1 == 'e')col = 5;
	else if (x1 == 'c')col = 3;  else if (x1 == 'g')col = 7;
	else if (x1 == 'd')col = 4;  else if (x1 == 'h')col = 8;

	if (x2 == 'a')col2 = 1;      else if (x2 == 'd')col2 = 4;
	else if (x2 == 'b')col2 = 2; else if (x2 == 'e')col2 = 5;
	else if (x2 == 'c')col2 = 3; else if (x2 == 'g')col2 = 7;
	else if (x2 == 'f')col2 = 6; else if (x2 == 'h')col2 = 8;

	//checks if its opponent's piece
	if (board[y][col] >= 'A' && board[y][col] <= 'Z') return false;

	//valid range of numbers for destination index
	if (y > 8 || y < 1 || y2 > 8 || y2 < 1) return false;

	//checks that x is in range
	if (x1 <'a' || x1 >'h' || x2 < 'a' || x2 > 'h')
		return false;

	////////////////////////validating according to chess rules////////////////////////
	//if one of your peices is there
	string myPieces = "prnbkq";
	for (int i = 0; i < myPieces.size(); i++)
		if (board[y2][col2] == myPieces[i]) {
			return false;
		}
	//empty position
	if (board[y][col] == 'e')
		return false;

	//X AND Y DIFFERENCES
	int rowDiff = y - y2; // moving among the 1-8 list (rows)
	//int colDiff = col - col2;// moving through a-h list (columns)
	int colDiff = col2 - col;

	 //~~~~~~~~~~~~~~~~~~~~~~~~PAWN RULES~~~~~~~~~~~~~~~~~~~~~~~~
	if (board[y][col] == 'p') {

		if (rowDiff == 2 && y == 7 && colDiff == 0) {
			if (board[y - 1][col] != 'e')
				return false;
			return true;
		}
		if (rowDiff == 2 && y != 7) return false;
		if (rowDiff != 1) return false;

		if (abs(colDiff) == 1 && abs(rowDiff) == 1) {
			string enemy = "PRNBKQ"; int cnt = 0;
			for (int i = 0; i < enemy.size(); i++) {
				if (board[y2][col2] == enemy[i]) cnt++;
			}
			if (cnt == 1) return true;
			else return false;
		}
		if (colDiff == 0 && abs(rowDiff) == 1 && board[y2][col2] != 'e') return false;
	}
	//~~~~~~~~~~~~~~~~~~~~~~~~KING RULES~~~~~~~~~~~~~~~~~~~~~~~~
	if (board[y][col] == 'k') {
		if (abs(rowDiff) > 1 || abs(colDiff) > 1) return false;

		else if (abs(rowDiff) == 1 || abs(rowDiff) == 0) {
			if (abs(colDiff) == 1 || abs(colDiff) == 0) {
				if (board[y2][col2] == 'e' || (board[y2][col2] >= 'A' && board[y2][col2] <= 'Z')) return true;
			}
		}
		else if (abs(rowDiff) == 1 && abs(colDiff) == 1)
			if (board[y2][col2] == 'e' || (board[y2][col2] >= 'A' && board[y2][col2] <= 'Z')) return true;

	}
	//~~~~~~~~~~~~~~~~~~~~~~~~QUEEN RULES~~~~~~~~~~~~~~~~~~~~~~~~
	if (board[y][col] == 'q') {
		//if they just try to move to some random spot
		if (abs(rowDiff) != abs(colDiff) && rowDiff != 0 && colDiff != 0)  return false;

		//COLLISION CHECKING IF STATEMENTS// yet to be handled
		else if (rowDiff > 0 && colDiff > 0) {
			cout << "lets see the y-abs y2-y and col+1 = " << abs(y2 - y) << " " << col + 1 << endl;
			for (int i = y; i > y2; i--) {

				for (int j = col - 1; j > col2; j--) {

					if (board[i][j] != 'e') {

						cout << "There's a piece blocking your move!" << endl << endl;
						return false;

					}
				}
			}
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~KNIGHT RULES~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~BISHOP RULES~~~~~~~~~~~~~~~~~~~~~~~~
	//~~~~~~~~~~~~~~~~~~~~~~~~ROCK RULES~~~~~~~~~~~~~~~~~~~~~~~~~~
	if (board[y][col] == 'r') {

		//if (rowDiff != 0 || colDiff != 0) return false; //moving stright in any direction 

		 if (rowDiff > 0 && colDiff == 0) { //up
			for (int i = y - 1; i >= y2; i--) {
				if (board[i][col] != 'e') {
					cout << "some peice is blocking your way!" << endl;
					return false;
				}
			}
			}
			else if (rowDiff < 0 && colDiff == 0) { //down
				for (int i = y + 1; i <= y2; i++) {
					if (board[i][col] != 'e') {
						cout << "some peice is blocking your way!" << endl;
						return false;
					}
				}
			}
			else if (rowDiff == 0 && colDiff > 0) { //right
				for (int i = col + 1; i <= col2; i++) {
					if (board[i][col] != 'e') {
						cout << "some peice is blocking your way!" << endl;
						return false;
					}
				}
			}
			else if (rowDiff == 0 && colDiff < 0) { //left
				for (int i = col - 1; i >= col2; i--) {
					if (board[i][col] != 'e') {
						cout << "some peice is blocking your way!" << endl;
						return false;
					}
				}
			}
			//else if (rowDiff = 0 && colDiff != 0) {
				//cout << "i swear rock doesnt move this way " << endl;
				//return false;
			//}
	}
	
	return true;
}
//=========================================================================================================================//
void arduino_fn() {

	//SerialPort arduino(port_name);
	//if (arduino.isConnected()) cout << "Connection Established" << endl;
	//else cout << "ERROR, check port name";

	if (arduino.isConnected()) {
		string moves="";
		
		moves.push_back(x1); moves.push_back(y + '0'); moves.push_back(x2); moves.push_back(y2 + '0');

		char *c_string = new char[moves.size() + 1];
		for (int i = 0; i < moves.size(); i++)
			c_string[i] = moves[i];

		//cout << c_string <<endl; Sleep(5000);
		//cout << moves.size() << endl;
		c_string[moves.size()] = '\n';

		arduino.writeSerialPort(c_string, MAX_DATA_LENGTH);

	//	arduino.readSerialPort(output, MAX_DATA_LENGTH);

		//puts(output);
		//Sleep(5000);
		delete[] c_string;
		//c_string = NULL;

	}
}
//=========================================================================================================================//
int main() {

	cout << "  ####################" << endl;
	cout << "  # 1-Beginner level #" << endl;
	cout << "  ####################" << endl;
	cout << "  ####################" << endl;
	cout << "  # 2-Advanced level #" << endl;
	cout << "  ####################" << endl;
	int level; cin >> level;
	system("cls");

	/////////////////BEGINNER LEVEL///////////////////
	if (level == 1) {
		while (true) {
			draw();
			if (player == 2) {
				cout << "Role of player 2" << endl;
				cout << "please entre your move: ";
				bool valid;
				do {
					cin >> x1 >> y >> x2 >> y2;
					valid = check_legal();
					if (valid == true) {
						move();
						draw();
						//arduino_fn();
						player = 1;
						//Sleep(10000);
					}
					else {
						cout << "Invalid! re-entre your move: ";
					}
				} while (valid != true);
			}

			if (player == 1) {
				cout << "Role of player 1" << endl;
				//check();
				//draw();
				cout << "pc is playing "; Sleep(500);
				//move();
				player = 2;
			}
			//arduino();
			//Sleep(1000);
		}
	}
	///////////////////ADVANCED LEVEL///////////////////
	else if (level == 2) {
		draw();
		if (player == 2) {
			cout << "Role of player 2" << endl;
			cout << "please entre your move: ";
		flag2:
			cin >> x1 >> y >> x2 >> y2;
			bool valid = check_legal();
			if (valid == true) {
				move();
				draw();
				player = 1;
			}
			else {
				cout << "Invalid move! re-entre your move: ";
				goto flag2;
			}
		}
		if (player == 1) {
			cout << "Role of player 1" << endl;
			cout << "pc is playing ";
			//stockfish();
			//move();
			//draw();
			player = 2;
		}

	}

	/////////////////ADDED FEATURES///////////////


	return 0;
}